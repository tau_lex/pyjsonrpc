from setuptools import find_packages
from setuptools import setup

with open("README.md") as readme_file:
    readme = readme_file.read()

requirements = ["PyJWT>=2.3.0"]

setup(
    name="pyjsonrpc",
    version="0.2.12",
    description="A package for simple implementation JsonRPC server",
    license="Apache License 2.0",
    author="Aleksey Terentyev",
    author_email="terentyev.a@pm.me",
    url="https://gitlab.com/tau_lex/pyjsonrpc",
    packages=find_packages(),
    install_requires=requirements,
    long_description=readme,
    long_description_content_type="text/markdown",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: Apache Software License 2.0",
        "Programming Language :: Python :: 3",
    ],
)
