from jsonrpc.decorators import remote_procedure


@remote_procedure(name="test_app.method", provide_request=True)
def test(request):
    return f"request({request.user}, )"


@remote_procedure(name="test_app.private_method", provide_request=True, authenticated=True)
def test(request):
    return f"request({request.user}, )"
