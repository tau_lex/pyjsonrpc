# PyJsonRpc

[WIP] Important! This module is under development and its interface is subject to change

Implementation of the Json RPC 2.0 protocol with support for JWT authentication and Django applications.

![license: Apache License 2.0](https://img.shields.io/badge/license-Apache%202-blue) ![Python: >=3.6](https://img.shields.io/badge/python-%3E=3.6-blue) ![Auth: JWT](https://img.shields.io/badge/auth-JWT-black) ![Code style: black](https://img.shields.io/badge/code%20style-black-black)

## Installation

```shell
pip install git+https://gitlab.com/tau_lex/pyjsonrpc.git@master
```

## Quick use

```python
from jsonrpc import rpc_registry
from jsonrpc.decorators import remote_procedure

@remote_procedure
def my_method():
	return "Hello Bro! ✋️"

print(rpc_registry.handle_request({"jsonrpc":"2.0", "method": "my_method", "id": 1}))
```

## Using

Examples:
```python
from jsonrpc.decorators import remote_procedure

@remote_procedure
def public_procedure():
	return "You called \"public_procedure\".️"

@remote_procedure(name="public.procedure")
def reassigned_name():
	return "You called \"reassigned_name\" as \"public.procedure\".️"

@remote_procedure(name="public.request", provide_request=True)
def public_procedure_with_request(request):
	return f"You called \"public.request\". Request obj️: {request}."

@remote_procedure
def private_procedure(authenticated=True):
	return "You called \"private_procedure\".️"
```

## Using with Django

1. Create file `rpc_methods.py` in your django application.
   Define function and wrap it `remote_procedure` decorator:
   ```python
   from jsonrpc.decorators import remote_procedure

   @remote_procedure
   def my_method(test: str):
       return "Hello Bro! ✋️"
   ```
2. Add `JsonRPCView` class to urlpatterns. The `as_view` method must accept the `autodiscover`
   argument as the name of the remote procedure files.
   ```python
   from django.urls import path
   from jsonrpc.django.views import JsonRPCView

   urlpatterns = [
       path("api/", JsonRPCView.as_view(autodiscover="rpc_methods")),
   ]
   ```

#### JWT Authentication in your Django project

1. Add `jsonrpc.django` module in INSTALLED_APPS list in the `settings.py` file.
2. Define `JSONRPC_SIGNING_KEY = "Some_Secret_String"` in the `settings.py` file
3. Use `token`, `token.refresh` and `token.verify` JsonRPC methods for authentication by JWT tokens.
