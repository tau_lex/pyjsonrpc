from django.apps import AppConfig


class JsonRpcConfig(AppConfig):
    name = "jsonrpc.django"
    verbose_name = "JSON-RPC"
