import logging
from typing import Protocol
from typing import Union

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth import get_user_model
from django.contrib.auth.middleware import get_user as get_django_user
from django.contrib.auth.models import AnonymousUser
from django.http import HttpRequest

from jsonrpc.config import rpc_config
from jsonrpc.exceptions import AuthenticationFailed
from jsonrpc.jwt import get_user_id

log = logging.getLogger(__name__)
UserModel = get_user_model()


class GetUserProtocol(Protocol):
    def __call__(self, request: HttpRequest) -> AbstractBaseUser:
        ...


def get_user(request: HttpRequest) -> Union[AbstractBaseUser, AnonymousUser]:
    user = get_django_user(request)
    if user.is_authenticated:
        return user

    return get_only_jwt_user(request)


def get_only_jwt_user(request: HttpRequest) -> Union[AbstractBaseUser, AnonymousUser]:
    auth_header = request.META.get("HTTP_AUTHORIZATION", "")
    auth_pair = auth_header.split()

    if len(auth_pair) == 0:
        # Empty AUTHORIZATION header sent
        return AnonymousUser()
    elif auth_pair[0] not in rpc_config.AUTH_HEADER_TYPES:
        # Assume the header does not contain a JSON web token
        return AnonymousUser()
    elif len(auth_pair) != 2:
        raise AuthenticationFailed("Authorization header must contain two space-delimited values")

    user_id = get_user_id(auth_pair[1])
    try:
        user = UserModel.objects.get(id=user_id)
    except UserModel.DoesNotExist:
        raise AuthenticationFailed("User not found")
    except Exception as err:
        log.exception(err)
        raise AuthenticationFailed()

    if not user.is_active:
        raise AuthenticationFailed("User is inactive")

    return user


# fixme: remove this alias
get_jwt_user = get_user
