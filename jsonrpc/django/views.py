import logging
from typing import Callable
from typing import ClassVar
from typing import Optional

from django.conf import settings
from django.http import HttpRequest
from django.http import HttpResponse
from django.utils.module_loading import autodiscover_modules
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

try:
    import ujson as json
except ImportError:
    import json

from jsonrpc import rpc_registry
from jsonrpc.config import parse_settings
from jsonrpc.django.utils import get_user
from jsonrpc.django.utils import GetUserProtocol
from jsonrpc.exceptions import InvalidRequest
from jsonrpc.exceptions import JsonRpcError
from jsonrpc.exceptions import ParseError

log = logging.getLogger(__name__)


class JsonRPCView(View):
    """Django class based view implements JSON-RPC"""

    content_type: ClassVar[str] = "application/json"
    get_user: ClassVar[GetUserProtocol] = None

    @classmethod
    def as_view(
        cls,
        autodiscover: str = "rpc_methods",
        settings_prefix: str = "JSONRPC",
        get_user_callback: Optional[GetUserProtocol] = None,
        **initkwargs,
    ) -> Callable:
        """Django class based view implements JSON-RPC.

        :param autodiscover: Submodule name(s) where defined RPC methods
        :type autodiscover: str
        :param settings_prefix: Prefix to search for JsonRPC module settings in
                                django config file
        :type settings_prefix: str
        :param get_user_callback: A function that returns a user object
        :type get_user_callback: GetUserProtocol
        :param initkwargs:
        :return:
        """
        # Configure jsonrpc module
        parse_settings(settings_object=settings, prefix=settings_prefix)
        if get_user_callback and callable(get_user_callback):
            cls.get_user = lambda s, r: get_user_callback(r)

        view = super().as_view(**initkwargs)
        view.cls = cls
        view.initkwargs = initkwargs

        # Scan specified sub-modules
        autodiscover_modules(autodiscover)

        return csrf_exempt(view)

    def http_method_not_allowed(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        """"""
        msg = f"HTTP Method Not Allowed ({request.method}): {request.path}"
        content = json.dumps(InvalidRequest(msg).as_dict())
        log.error(f"{msg}; Response={content};")

        return HttpResponse(content=content, content_type=self.content_type)

    def post(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        try:
            log.debug(f"JsonRPCView.post; Request={request.body}; Headers={request.headers};")
            # Check request Content-Type and data
            if request.content_type != self.content_type or len(request.body) == 0:
                raise InvalidRequest()

            # Handle request object
            request.data = json.loads(request.body)
            if self.get_user is not None:
                request.user = self.get_user(request)
            else:
                request.user = get_user(request)
            log.debug(f"JsonRPCView.post; User={request.user};")

            content = ""
            result = rpc_registry.handle_request(
                json_data=request.data,
                request=request,
            )
            if result:
                content = json.dumps(result, ensure_ascii=False)
        except JsonRpcError as err:
            content = json.dumps(err.as_dict())
            log.exception(f"{err}; Response={content};")
        except ValueError as err:
            content = json.dumps(ParseError().as_dict())
            log.exception(f"{err}; Response={content};")
        except Exception as err:
            content = json.dumps(JsonRpcError().as_dict())
            log.exception(f"{err}; Response={content};")

        return HttpResponse(content=content, content_type=self.content_type)
