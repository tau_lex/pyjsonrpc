from functools import wraps
from inspect import getfullargspec
from typing import Callable

from jsonrpc import rpc_registry

# Reserved parameter names
reserved_kwargs = ("self", "request")


def remote_procedure(*args, **kwargs) -> Callable:
    """Decorator allow wrap function and define it as remote procedure.

    name : str
        Procedure name. Default as function name.
    authenticated : bool
        Flag for private functions.
    provide_request : bool
        Function may receive request object.

    """

    def create_decorator(*params, **options) -> Callable:
        """Create a decorator that will return the wrapped function"""

        def decorator(func: Callable) -> Callable:
            """Returns a wrapper with a wrapped function"""

            @wraps(func)
            def wrapper(*f_args, **f_kwargs):
                """"""
                result = func(*f_args, **f_kwargs)
                return result

            procedure_name = options.get("name")
            if procedure_name:
                wrapper.__name__ = procedure_name

            wrapper.__rpc__ = True
            wrapper._authenticated = options.get("authenticated", False)
            wrapper._provide_request = options.get("provide_request", False)
            wrapper._args = [a for a in getfullargspec(func).args if a not in reserved_kwargs]

            if False:  # if func.is_method():
                rpc_registry.register_class_methods(wrapper)  # noqa
            # mb: if not custom_registry:
            rpc_registry.register_procedure(wrapper)

            wrapper: func
            return wrapper

        return decorator

    if len(args) == 1 and callable(args[0]):
        return create_decorator()(args[0])
    return create_decorator(*args, **kwargs)
