from typing import Any
from typing import Dict
from typing import Optional
from typing import Union


class JsonRpcError(Exception):
    """JSON-RPC Common error

    Reserved for implementation-defined server-errors.
    Codes -32000 to -32099.

    """

    code = -32000
    default_message = "Server error"
    _custom_message = ""

    def __init__(
        self,
        message: str = None,
        id: Union[int, str] = None,
        details: Optional[Any] = None,
        *args,
        **kwargs,
    ):
        if message:
            self._custom_message = ": " + message.strip()

        self._id = id
        self._details = details

        super().__init__(*args, **kwargs)

    def __str__(self) -> str:
        return self.default_message

    def as_dict(self) -> Dict[str, Union[str, int, Dict[str, Union[int, str, Any]]]]:
        msg = self.default_message + self._custom_message
        dict_obj = dict(
            jsonrpc="2.0",
            error=dict(code=self.code, message=msg),
            id=self._id,
        )

        if self._details:
            dict_obj["error"].update(details=self._details)

        return dict_obj


class InvalidRequest(JsonRpcError):
    """Invalid Request

    The JSON sent is not a valid Request object.

    """

    code = -32600
    default_message = "Invalid Request"


class MethodNotFound(JsonRpcError):
    """Method not found

    The method does not exist / is not available.

    """

    code = -32601
    default_message = "Method not found"


class InvalidParams(JsonRpcError):
    """Invalid params

    Invalid method parameter(s).

    """

    code = -32602
    default_message = "Invalid params"


class InternalError(JsonRpcError):
    """Internal error

    Internal JSON-RPC error.

    """

    code = -32603
    default_message = "Internal error"


class ParseError(JsonRpcError):
    """Parse error

    Invalid JSON was received by the server.
    An error occurred on the server while parsing the JSON text.

    """

    code = -32700
    default_message = "Parse error"


# JSON-RPC Module exceptions


class AuthenticationFailed(JsonRpcError):
    """Authentication failed"""

    code = -32001
    default_message = "Authentication failed"
