from jsonrpc.exceptions import AuthenticationFailed
from jsonrpc.exceptions import JsonRpcError


class JwtTokenError(JsonRpcError):
    """"""

    code = -32001
    default_message = "JWT Token Error"


class TokenBackendError(JsonRpcError):
    """"""

    code = -32001
    default_message = "Token Backend Error"


class InvalidToken(AuthenticationFailed):
    """"""

    code = -32001
    default_message = "Invalid Token"
