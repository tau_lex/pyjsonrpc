from datetime import datetime

try:
    from zoneinfo import ZoneInfo

    UTC = ZoneInfo("UTC")
except (ImportError, KeyError):
    try:
        from pytz import utc as UTC
    except ImportError:
        from datetime import timezone

        UTC = timezone.utc
from typing import Any
from typing import Dict
from uuid import uuid4

from jsonrpc.config import rpc_config
from jsonrpc.jwt.exceptions import JwtTokenError


def utc_now_aware():
    return datetime.now(tz=UTC)


def datetime_to_timestamp(dt):
    return int(dt.timestamp())


def datetime_from_timestamp(ts):
    return datetime.fromtimestamp(ts, UTC)


def _set_exp(payload: Dict[str, Any], current_time, lifetime, claim="exp"):
    """
    Updates the expiration time of a token.
    See here:
    https://tools.ietf.org/html/rfc7519#section-4.1.4
    """
    payload[claim] = datetime_to_timestamp(current_time + lifetime)


def _check_exp(payload: Dict[str, Any], current_time, claim="exp"):
    """
    Checks whether a timestamp value in the given claim has passed (since
    the given datetime value in `current_time`). Raises a TokenError with
    a user-facing error message if so.
    """
    try:
        claim_value = payload[claim]
    except KeyError:
        raise JwtTokenError(f"Token has no '{claim}' claim")

    claim_time = datetime_from_timestamp(claim_value)
    if claim_time <= current_time:
        raise JwtTokenError(f"Token '{claim}' claim has expired")


def _set_iat(payload: Dict[str, Any], current_time, claim="iat"):
    """
    Updates the time at which the token was issued.
    See here:
    https://tools.ietf.org/html/rfc7519#section-4.1.6
    """
    payload[claim] = datetime_to_timestamp(current_time)


def _set_jti(payload: Dict[str, Any]):
    """
    Populates the configured jti claim of a token with a string where there
    is a negligible probability that the same string will be chosen at a
    later time.
    See here:
    https://tools.ietf.org/html/rfc7519#section-4.1.7
    """
    payload[rpc_config.JTI_CLAIM] = uuid4().hex
