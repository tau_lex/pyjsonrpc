"""
JsonRPC sub module for JWT authentication
"""
import logging
from typing import Dict
from typing import Optional
from typing import Union

from jsonrpc.config import rpc_config
from jsonrpc.jwt.exceptions import InvalidToken
from jsonrpc.jwt.exceptions import JwtTokenError
from jsonrpc.jwt.exceptions import TokenBackendError
from jsonrpc.jwt.tokens import AccessToken
from jsonrpc.jwt.tokens import decode
from jsonrpc.jwt.tokens import RefreshToken
from jsonrpc.jwt.utils import _check_exp
from jsonrpc.jwt.utils import _set_exp
from jsonrpc.jwt.utils import _set_iat
from jsonrpc.jwt.utils import _set_jti
from jsonrpc.jwt.utils import utc_now_aware

log = logging.getLogger(__name__)


def get_user_id(token: str) -> int:
    validated_token = AccessToken(token)
    try:
        return validated_token[rpc_config.USER_ID_CLAIM]
    except KeyError:
        raise InvalidToken("Token contained no recognizable user identification")


def get_tokens_by(refresh, rotate_refresh: bool = False) -> Dict[str, str]:
    refresh = RefreshToken(refresh)

    result = {"access": str(refresh.access_token)}

    if rotate_refresh:
        _set_jti(refresh.payload)
        _set_exp(refresh.payload, utc_now_aware(), rpc_config.REFRESH_TOKEN_LIFETIME)
        _set_iat(refresh.payload, utc_now_aware())

        result["refresh"] = str(refresh)

    return result


def get_tokens_for(
    user_id: Union[int, str],
    payload: Optional[Dict[str, Union[bool, int, str]]] = None,
) -> Dict[str, str]:
    refresh = RefreshToken.for_user(user_id)

    if payload:
        for k, v in payload.items():
            if not isinstance(v, (bool, int, str, bytes)):
                raise JwtTokenError(f"Bad payload value type: {type(v)}({v})")
            refresh[k] = str(v)

    result = {
        "refresh": str(refresh),
        "access": str(refresh.access_token),
    }
    return result


def verify_tk(token: str):
    """

    :param token: Token for verification
    :except JwtTokenError: raised when the token is not valid
    """
    """
    !!!! IMPORTANT !!!! MUST raise a TokenError with a user-facing error
    message if the given token is invalid, expired, or otherwise not safe
    to use.
    """
    current_time = utc_now_aware()

    # Decode token
    try:
        payload = decode(token, verify=True)
    except TokenBackendError:
        raise JwtTokenError("Token is invalid or expired")

    """
    Performs additional validation steps which were not performed when this
    token was decoded. This method is part of the "public" API to indicate
    the intention that it may be overridden in subclasses.
    """
    # According to RFC 7519, the "exp" claim is OPTIONAL
    # (https://tools.ietf.org/html/rfc7519#section-4.1.4).  As a more
    # correct behavior for authorization tokens, we require an "exp"
    # claim.  We don't want any zombie tokens walking around.
    _check_exp(payload, current_time)

    # Ensure token id is present
    if rpc_config.JTI_CLAIM not in payload:
        raise JwtTokenError("Token has no id")
