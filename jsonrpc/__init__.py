import logging
from typing import Any
from typing import Callable
from typing import Dict
from typing import List
from typing import Optional
from typing import Union

from jsonrpc.exceptions import AuthenticationFailed
from jsonrpc.exceptions import InvalidRequest
from jsonrpc.exceptions import JsonRpcError
from jsonrpc.exceptions import MethodNotFound

log = logging.getLogger(__name__)


class JsonRpc:
    _remote_procedures: Dict[str, Callable] = {}

    def register_procedure(self, function: Callable):
        """Register a wrapped function as a remote procedure"""
        if not getattr(function, "__rpc__", False):
            return

        self._remote_procedures[function.__name__] = function

    def handle_request(
        self,
        json_data: Union[dict, List[dict]],
        request: Any = None,
        **kwargs,
    ) -> Union[dict, List[Optional[dict]]]:
        """Handle procedure call or batch procedure calls

        :param json_data: Union[dict, list]: Request data.
        :param request: Any: Optional parameter that can be passed as an argument to the procedure.
        :returns: Union[dict, list]:
        """
        try:
            # Handle batch request
            if isinstance(json_data, list):
                if len(json_data) == 0:
                    raise InvalidRequest()

                response = []
                for proc_call in json_data:
                    if isinstance(proc_call, dict):
                        try:
                            result = self.perform(proc_call, request)
                            if result:
                                response.append(result)
                        except JsonRpcError as err:
                            rpc_err = err.as_dict()
                            log.error(f"{err}; Response={rpc_err};")
                            response.append(rpc_err)
                    else:
                        rpc_err = InvalidRequest().as_dict()
                        log.error(f"Request is not JSON object; Response={rpc_err};")
                        response.append(rpc_err)

                return response

            # Handle single request
            elif isinstance(json_data, dict):
                return self.perform(json_data, request)

            # Invalid request
            else:
                raise InvalidRequest()
        except JsonRpcError as err:
            rpc_err = err.as_dict()
            log.error(f"{err}; Response={rpc_err};")

            return rpc_err

    def perform(
        self,
        proc_call: Dict[str, Union[str, Any]],
        request: Any = None,
    ) -> Optional[Dict[str, Union[str, Any]]]:
        rpc: str = proc_call.get("jsonrpc")
        method: str = proc_call.get("method")
        params: Optional[list, dict] = proc_call.get("params")
        _id: Optional[Union[int, str]] = proc_call.get("id")

        # Validate procedure call
        if (
            rpc != "2.0"
            or not isinstance(method, str)
            or (params and not isinstance(params, (list, dict)))
        ):
            raise InvalidRequest(id=_id)

        # Getting procedure
        procedure = self._remote_procedures.get(method)
        if not procedure:
            if _id is None:
                return None
            raise MethodNotFound(id=_id)
        elif getattr(procedure, "_authenticated", False):
            # todo: think about this checking
            if not request.user.is_authenticated:
                # todo: change to "for authenticated users"
                raise AuthenticationFailed(f"User={request.user}", id=_id)

        # Prepare parameters
        args, kwargs = [], {}
        if getattr(procedure, "_provide_request", False):
            kwargs.update(request=request)
        if params and isinstance(params, list):
            args = params
        elif params and isinstance(params, dict):
            kwargs.update(params)

        # Execute RPC method
        result = procedure(*args, **kwargs)
        if _id is None:
            return None

        return dict(jsonrpc="2.0", result=result, id=_id)


rpc_registry = JsonRpc()
